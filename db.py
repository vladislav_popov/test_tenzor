import psycopg2

import config


class DB:
    def __init__(self):
        self.db_name = config.db_name
        self.db_user = config.db_user
        self.db_password = config.db_password
        self.fill()

    def create_connection(self):
        return psycopg2.connect(f'dbname={self.db_name} user={self.db_user} password={self.db_password}')

    def fill(self):
        conn = self.create_connection()
        with conn:
            with conn.cursor() as cur:
                cur.execute("DROP TABLE organization")
                cur.execute(
                    "CREATE TABLE organization (id INT PRIMARY KEY, ParentId INT, Name VARCHAR (300), Type INT);")
                for data in config.db_json:
                    cur.execute("INSERT INTO organization (id, ParentId, Name, Type) VALUES (%s, %s, %s, %s)",
                                (data['id'], data['ParentId'], data['Name'], data['Type']))
                conn.commit()

    def find_very_parent(self, id):
        conn = self.create_connection()
        with conn:
            with conn.cursor() as cur:
                cur.execute("SELECT id, ParentId, Name FROM organization WHERE id=%s", (id,))
                result = cur.fetchone()
                if result:
                    cur_id, parent_id, name = result
                else:
                    raise ValueError(f'Родитель для id: {id} не найден')
        if parent_id is not None:
            cur_id, name = self.find_very_parent(parent_id)
        return cur_id, name

    def find_children(self, parent_id):
        conn = self.create_connection()
        with conn:
            with conn.cursor() as cur:
                cur.execute("SELECT id, Type, Name from organization WHERE ParentId=%s", (parent_id,))
                for line in cur.fetchall():
                    cur_id, cur_type, name = line
                    if cur_type == 3:
                        print(name)
                    else:
                        self.find_children(cur_id)

    def process(self, id):
        parent_id, name = self.find_very_parent(id)
        print(f'Офис выбранного сотрудника: {name}')
        print('Сотрудники:')
        self.find_children(parent_id)
        # self.find_children(11)


try:
    id = int(input('Введите id сотрудника: '))
except ValueError:
    raise ValueError('Введенный id должен быть числом')

db = DB()
db.process(id)
